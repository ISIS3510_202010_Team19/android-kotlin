package com.example.codabapp.ui.clientes

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codabapp.R
import com.example.codabapp.ui.utils.CircleTransform
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recyclerview_item_cliente.view.*

class ClienteListAdapter internal constructor(context: Context, pClienteListener: OnClienteListener):
                            RecyclerView.Adapter<ClienteListAdapter.ClienteViewHolder>(){

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var clientes = emptyList<Cliente>()     //Cached copy of clientes

    private val onClienteListener: OnClienteListener = pClienteListener

    inner class ClienteViewHolder(itemView: View, pClientListener: OnClienteListener):
        RecyclerView.ViewHolder(itemView), OnClickListener{

        val clienteNombre: TextView = itemView.nombre_clienteView
        val clienteDireccion: TextView = itemView.direccion_clienteView
        val clienteTelefono: TextView = itemView.telefono_clienteView
        val clienteFotoPerfil: ImageView = itemView.iconCliente

        private val onClienteListener: OnClienteListener = pClientListener

        init{
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onClienteListener.onClienteClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClienteViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item_cliente, parent, false)
        return ClienteViewHolder(itemView, onClienteListener)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ClienteViewHolder, position: Int){
        val current = clientes[position]
        holder.clienteNombre.text = "${current.nombre } ${current.apellido}"
        holder.clienteDireccion.text = current.direccion
        holder.clienteTelefono.text = current.telefono
        Picasso.get().load(current.fotoPerfil).resize(100,100)
            .centerCrop().transform(CircleTransform()).into(holder.clienteFotoPerfil)
    }

    internal fun setClientes(clientes: List<Cliente>){
        this.clientes = clientes
        notifyDataSetChanged()
    }

    override fun getItemCount() = clientes.size

    //Fuente https://youtu.be/69C1ljfDvl0
    interface OnClienteListener{
        fun onClienteClick(pos: Int)
    }

}