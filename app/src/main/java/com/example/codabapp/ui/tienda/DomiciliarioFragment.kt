package com.example.codabapp.ui.tienda

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codabapp.R
import androidx.lifecycle.Observer
import com.example.codabapp.ui.tienda.DomiciliarioListAdapter.OnDomiciliarioListener
import kotlinx.android.synthetic.main.fragment_tienda.*

class DomiciliarioFragment : Fragment(), OnDomiciliarioListener {

    //List adapter
    private lateinit var adapter: DomiciliarioListAdapter

    private lateinit var domiciliarioViewModel: DomiciliarioViewModel

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        domiciliarioViewModel = ViewModelProvider(this).get(DomiciliarioViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        agregar_domiciliariomas.setOnClickListener {
            Log.d("Mariana", "entra a setOnClickListener")
            val fr: FragmentTransaction = fragmentManager!!.beginTransaction()
            fr.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            fr.replace(R.id.domiciliarios_fragment_view, AgregarDomiciliarioFragment.newInstance())
            fr.addToBackStack(null)
            fr.commit()
        }

        //RecyclerView
        if(savedInstanceState == null) {
            val recyclerView = recyclerview_domiciliarios
            adapter = DomiciliarioListAdapter(this.context!!, this)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(this.context!!)

            domiciliarioViewModel = ViewModelProvider(this).get(DomiciliarioViewModel::class.java)
            domiciliarioViewModel.allDomiciliarios.observe(viewLifecycleOwner, Observer { pedidos ->
                pedidos?.let { adapter.setDomiciliarios(it) }
            })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tienda, container, false)
    }

    override fun onDomiciliarioClick(pos: Int) {
        val sel: Domiciliario = domiciliarioViewModel.getDomiciliarioByPos(pos)

        val intent = Intent(context, DetalleDomiciliario::class.java)
        intent.putExtra("name", "${sel.nombre} ${sel.apellido}")
        intent.putExtra("telefono", sel.telefono)
        intent.putExtra("foto", sel.fotoPerfil)
        startActivity(intent)
    }
}
