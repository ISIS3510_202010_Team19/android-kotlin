package com.example.codabapp.ui.login


import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.codabapp.R
import com.example.codabapp.ui.utils.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sing_up.*



class SingUpActivity : AppCompatActivity() {

    private val mAuth: FirebaseAuth by lazy{ FirebaseAuth.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sing_up)

        button_backLoggin.setOnClickListener {
            goToActivity<LoginActivity>{
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
           // startActivity(Intent(this, LoginActivity::class.java))
        }

        button_loginNew.setOnClickListener {

            val email =editText_emailNew.text.toString()
            val password = editText_passwordNew.text.toString()
            val confirmPassword = editText_confirmpassword.text.toString()
            if(isValidateEmail(email) && isValidatePassword(password) && isValidateConfirmPassword(password, confirmPassword)){
                signUpByEmail( email, password )
            }
            else{ Toast.makeText(this, "Por favor asegúrate que toda los datos sean correctos", Toast.LENGTH_LONG).show()
            }
        }

        editText_emailNew.validate {
            editText_emailNew.error= if (isValidateEmail(it)) null else "El email no es válido"
        }
        editText_passwordNew.validate {
            editText_passwordNew.error= if (isValidatePassword(it)) null else "La contraseña debe contener 1 letra mayúscula, 1 minúscula, 1 número, 1 caracter especial y 4 caracteres como mínimo"
        }
        editText_confirmpassword.validate {
            editText_confirmpassword.error= if (isValidateConfirmPassword(editText_passwordNew.text.toString(), it)) null else "Las contraseñas no son iguales"
        }

        val currentUser = mAuth.currentUser
        if(currentUser == null ) {
            Toast.makeText(this, "Por favor ingresa tus datos", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "El usuario está loggeado", Toast.LENGTH_LONG).show()

        }

    }

    private fun signUpByEmail(email: String, password:String){
        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    mAuth.currentUser!!.sendEmailVerification().addOnCompleteListener(this){
                        Toast.makeText(this, "Un email ha sido enviado, por favor confírmalo", Toast.LENGTH_LONG).show()
                    }
                    Toast.makeText(this, "Se creó al usuario exitosamente", Toast.LENGTH_LONG).show()
                    goToActivity<LoginActivity>{
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    }
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

                } else {

                    Toast.makeText(this, "Un error inesperado ocurrió, por favor intenta de nuevo", Toast.LENGTH_LONG).show()
                }
            }
         }




}

