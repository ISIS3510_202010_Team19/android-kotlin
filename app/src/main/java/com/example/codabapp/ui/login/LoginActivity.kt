package com.example.codabapp.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.codabapp.MainActivity
import com.example.codabapp.R
import com.example.codabapp.ui.utils.*
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener{

    private val mAuth:FirebaseAuth by lazy { FirebaseAuth.getInstance() }
    private val mGoogleApiClient: GoogleApiClient by lazy { getGoogleApiClient()}
    private val RC_GOOGLE_SIGN_IN = 99


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button_login.setOnClickListener {
            val email = editText_email.text.toString()
            val password = editText_password.text.toString()
            if(isValidateEmail(email) && isValidatePassword(password)){
                logInByEmail(email,password)
            }
            else{
                Toast.makeText(this, "Por favor asegúrate que todos los datos sean correctos", Toast.LENGTH_LONG).show()
            }
        }

        textForgotPassword.setOnClickListener{
            goToActivity<ForgotPasswordActivity>()
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        }

        button_createAccount.setOnClickListener {
            goToActivity<SingUpActivity> ()
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        }
        button_loginGoogle.setOnClickListener {
            val signInIntent= Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            startActivityForResult(signInIntent,100)
        }

        editText_email.validate {
            editText_email.error= if (isValidateEmail(it)) null else "El email no es valido"
        }
        editText_password.validate {
            editText_password.error= if (isValidatePassword(it)) null else "La contraseña debe contener 1 letra mayuscula, 1 minuscula, 1 numero, 1 caracter especial y 4 caracteres como minimo"
        }
    }

    private fun getGoogleApiClient():GoogleApiClient{
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        return GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
    }

    private fun loginByGoogleAccountIntoFirebase(googleAccount:GoogleSignInAccount){
        val credential = GoogleAuthProvider.getCredential(googleAccount.idToken, null)
        mAuth.signInWithCredential(credential).addOnCompleteListener(this){
            Toast.makeText(this, "Sign in by Google", Toast.LENGTH_LONG).show()
        }

    }

    private fun logInByEmail(email: String, password: String){
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
            if(task.isSuccessful){
                if(mAuth.currentUser!!.isEmailVerified){
                    Toast.makeText(this, "El usuario se ha loggeado", Toast.LENGTH_LONG).show()
                    val intent = Intent(this, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                else{
                    Toast.makeText(this, "El usuario debe confirmar el correo primero", Toast.LENGTH_LONG).show()
                }
            }
            else{
                Toast.makeText(this, "Un error inesperado ha ocurrido, por favor intente nuevamente", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == RC_GOOGLE_SIGN_IN){
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (result != null) {
                if(result.isSuccess){
                    val account = result.signInAccount
                    loginByGoogleAccountIntoFirebase(account!!)

                }
            }
        }
    }
    override fun onConnectionFailed(p0: ConnectionResult) {
        Toast.makeText(this, "La conexión fallo", Toast.LENGTH_LONG).show()
    }


}
