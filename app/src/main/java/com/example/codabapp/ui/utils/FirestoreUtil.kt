package com.example.codabapp.ui.utils

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore


object FirestoreUtil {

    private val firestoreInstance: FirebaseFirestore by lazy { FirebaseFirestore.getInstance()}
    private val store: FirebaseFirestore = FirebaseFirestore.getInstance()
    private lateinit var clientesDBRef : CollectionReference
    private lateinit var domiciliariosDBRef : CollectionReference


    fun updateCurrentUser(nombre: String = "", apellido: String = "", direccion: String = "", telefono: String = "", fotoPerfil: String? =null){
        val userFieldMap = mutableMapOf<String, Any>()
        if (nombre.isNotBlank()) userFieldMap["name"] = nombre
        if (apellido.isNotBlank()) userFieldMap["apellido"] = apellido
        if (direccion.isNotBlank()) userFieldMap["direccion"] = direccion
        if (telefono.isNotBlank()) userFieldMap["telefono"] = telefono
        if (fotoPerfil!= null) userFieldMap["fotoPerfil"] = fotoPerfil
        clientesDBRef.add(userFieldMap)

       // currentClientDocRef.update(userFieldMap)

    }
    fun updateCurrentDomiciliario(nombre: String = "", apellido: String = "", telefono: String = "", fotoPerfil: String? =null){
        val userFieldMap = mutableMapOf<String, Any>()
        if (nombre.isNotBlank()) userFieldMap["name"] = nombre
        if (apellido.isNotBlank()) userFieldMap["apellido"] = apellido
        if (telefono.isNotBlank()) userFieldMap["telefono"] = telefono
        if (fotoPerfil!= null) userFieldMap["fotoPerfil"] = fotoPerfil
        domiciliariosDBRef.add(userFieldMap)

    }
}