package com.example.codabapp.ui.clientes

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codabapp.R
import com.example.codabapp.ui.clientes.ClienteListAdapter.OnClienteListener
import kotlinx.android.synthetic.main.fragment_clientes.*

class ClientesFragment : Fragment(), OnClienteListener{

    //List adapter
    private lateinit var adapter: ClienteListAdapter

    private lateinit var clientesViewModel: ClienteViewModel

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        clientesViewModel = ViewModelProvider(this).get(ClienteViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_agregar_cliente.setOnClickListener {
            Log.d("Pablo", "entra a setOnClickListener")
            @Suppress("DEPRECATION") val fr: FragmentTransaction = fragmentManager!!.beginTransaction()
            fr.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            fr.replace(R.id.clientes_fragment_view, AgregarClienteFragment.newInstance())
            fr.addToBackStack(null)
            fr.commit()
        }

        //RecyclerView
        val recyclerView = recyclerview_clientes
        adapter = ClienteListAdapter(this.context!!, this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this.context!!)

        clientesViewModel = ViewModelProvider(this).get(ClienteViewModel::class.java)
        clientesViewModel.allClientes.observe(viewLifecycleOwner, Observer { pedidos ->
            pedidos?.let { adapter.setClientes(it) }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_clientes, container, false)
    }

    override fun onClienteClick(pos: Int) {
        val sel: Cliente = clientesViewModel.getClientByPosition(pos)

        val intent = Intent(context, DetalleCliente::class.java)
        intent.putExtra("name", "${sel.nombre} ${sel.apellido}")
        intent.putExtra("direccion", sel.direccion)
        intent.putExtra("telefono", sel.telefono)
        intent.putExtra("foto", sel.fotoPerfil)
        startActivity(intent)
    }
}
