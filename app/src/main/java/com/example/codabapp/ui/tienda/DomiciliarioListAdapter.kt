package com.example.codabapp.ui.tienda

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codabapp.R
import com.example.codabapp.ui.utils.CircleTransform
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recyclerview_item_domiciliario.view.*

class DomiciliarioListAdapter internal constructor(context: Context, pDomiciliarioListener: OnDomiciliarioListener):
                                RecyclerView.Adapter<DomiciliarioListAdapter.DomiciliarioViewHolder>(){

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var domiciliario = emptyList<Domiciliario>()     //Cached copy of domiciliarios

    private val onDomiciliarioListener: OnDomiciliarioListener = pDomiciliarioListener

    inner class DomiciliarioViewHolder(itemView: View, pDomiciliarioListener: OnDomiciliarioListener):
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        val domiciliarioNombre: TextView = itemView.nombre_domiciliarioView
        val domiciliarioTelefono: TextView = itemView.telefono_domiciliarioView
        val domiciliarioFotoPerfil: ImageView = itemView.iconDomi

        private val onDomiciliarioListener: OnDomiciliarioListener = pDomiciliarioListener

        init{
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onDomiciliarioListener.onDomiciliarioClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DomiciliarioViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item_domiciliario, parent, false)
        return DomiciliarioViewHolder(itemView, onDomiciliarioListener)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DomiciliarioViewHolder, position: Int){
        val current = domiciliario[position]
        holder.domiciliarioNombre.text = "${current.nombre } ${current.apellido}"
        holder.domiciliarioTelefono.text = current.telefono
        Picasso.get().load(current.fotoPerfil).resize(100,100)
            .centerCrop().transform(CircleTransform()).into(holder.domiciliarioFotoPerfil)
    }

    internal fun setDomiciliarios(domiciliarios: List<Domiciliario>){
        this.domiciliario = domiciliarios
        notifyDataSetChanged()
    }

    override fun getItemCount() = domiciliario.size

    //Fuente https://youtu.be/69C1ljfDvl0
    interface OnDomiciliarioListener {
        fun onDomiciliarioClick(pos: Int)
    }

}