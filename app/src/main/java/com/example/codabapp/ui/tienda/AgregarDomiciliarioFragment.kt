package com.example.codabapp.ui.tienda

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.codabapp.R
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_agregar_domiciliario.*
import kotlinx.android.synthetic.main.fragment_agregar_domiciliario.view.*
import java.util.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class AgregarDomiciliarioFragment: Fragment() {

    private var param1: String? = null
    private var param2: String? = null

    private val store: FirebaseFirestore = FirebaseFirestore.getInstance()
    private lateinit var domiciliariosDBRef : CollectionReference

    private val PICK_IMAGE_REQUEST = 71

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view=  inflater.inflate(R.layout.fragment_agregar_domiciliario, container, false)
        setDomiciliarioDB()
        view.apply {
            agregar_foto.setOnClickListener {

                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                startActivityForResult(intent, 0)

            }
            agregar_domiciliario.setOnClickListener {
                if(selectedImagePath== null){
                    val toast = Toast.makeText(activity, "Por favor agrega una imagen", Toast.LENGTH_SHORT)
                    toast.show()
                }
                uploadImageToFirebaseStorage()

            }
            tomar_foto.setOnClickListener {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)

                }
            }
        return view
    }

    var selectedImagePath : Uri?  = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null){
            Log.d("AgregarDomiciliarioFragment", "La foto fue seleccionada de la galeria")

            selectedImagePath = data.data
            val selectedImageBmp= MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedImagePath)

            val bitMapDrawable = BitmapDrawable(selectedImageBmp)
            agregar_foto_domiciliario.setBackgroundDrawable(bitMapDrawable)
        }
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null){
            Log.d("AgregarDomiciliarioFragment", "La foto fue tomada")
            selectedImagePath = data.data
            val selectedImageBmp= MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedImagePath)
            val bitMapDrawable = BitmapDrawable(selectedImageBmp)
            agregar_foto_domiciliario.setBackgroundDrawable(bitMapDrawable)
        }
    }

    private fun addDomiciliario(nombre: String = "", apellido: String = "", telefono: String = "", fotoPerfil: String? = null){
        val userFieldMap = mutableMapOf<String, Any>()
        if (nombre.isNotBlank()) userFieldMap["name"] = nombre
        if (apellido.isNotBlank()) userFieldMap["apellido"] = apellido
        if (telefono.isNotBlank()) userFieldMap["telefono"] = telefono
        if (fotoPerfil!= null) userFieldMap["fotoPerfil"] = fotoPerfil
        domiciliariosDBRef.add(userFieldMap)
            .addOnSuccessListener {
                val toast = Toast.makeText(activity, "Domiciliario agregado", Toast.LENGTH_SHORT)
                toast.show()
                activity!!.supportFragmentManager.popBackStackImmediate()
                Log.d("Agregar", "Se agrego el domiciliario ")
                //uploadImageToFirebaseStorage()
            }
            .addOnFailureListener {
                val toast = Toast.makeText(activity, "No se pudo agregar al domiciliario, intenta otra vez", Toast.LENGTH_SHORT)
                toast.show()
            }

        // currentClientDocRef.update(userFieldMap)

    }

    private fun setDomiciliarioDB(){
        domiciliariosDBRef= store.collection("domiciliario")
    }

    private fun uploadImageToFirebaseStorage(){
        if (selectedImagePath== null) return
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/fotosPerfil/$filename")

        ref.putFile(selectedImagePath!!)
            .addOnSuccessListener {
                Log.d("Agregar", "Se agrego bien la imagen: ${it.metadata?.path}")
                ref.downloadUrl.addOnSuccessListener {
                    it.toString()
                    Log.d("AgregarFoto", "FileLocation: $it")
                    saveDomiciliario(it.toString())
                }
            }
    }
    private fun saveDomiciliario(imagenPerfil: String) {
        if(editText_domiciliario== null || editText_nombreDom == null || editText_telefono == null ){
            val toast = Toast.makeText(activity, "Por favor ingresa los datos", Toast.LENGTH_SHORT)
            toast.show()
        }
        else{

        addDomiciliario(editText_nombreDom.text.toString(),
            editText_domiciliario.text.toString(),
            editText_telefono.text.toString(),imagenPerfil)}}


    companion object {
        @JvmStatic
        fun newInstance() =
            AgregarDomiciliarioFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
