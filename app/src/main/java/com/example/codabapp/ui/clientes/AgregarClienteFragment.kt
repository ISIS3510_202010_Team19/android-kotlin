package com.example.codabapp.ui.clientes

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.codabapp.R
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_agregar_cliente.*
import kotlinx.android.synthetic.main.fragment_agregar_cliente.view.*
import java.util.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class AgregarClienteFragment: Fragment() {

    private var param1: String? = null
    private var param2: String? = null

    private val store: FirebaseFirestore = FirebaseFirestore.getInstance()
    private lateinit var clientesDBRef : CollectionReference

    private val PICK_IMAGE_REQUEST = 71

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view=  inflater.inflate(R.layout.fragment_agregar_cliente, container, false)
        setClientDB()
        view.apply {
            agregar_foto.setOnClickListener {
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                startActivityForResult(intent, 0)
                }

            agregar_cliente.setOnClickListener {
                if(selectedImagePath== null){
                    val toast = Toast.makeText(activity, "Por favor agrega una imagen", Toast.LENGTH_SHORT)
                    toast.show()
                }
                uploadImageToFirebaseStorage()

            }
            tomar_foto.setOnClickListener {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
            }
        }
        return view
}
    private var selectedImagePath : Uri?  = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null){
            Log.d("AgregarClienteFragment", "La foto fue seleccionada de la galeria")

            selectedImagePath = data.data
            val selectedImageBmp= MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedImagePath)
            val bitMapDrawable = BitmapDrawable(selectedImageBmp)
            agregar_foto_cliente.setImageDrawable(bitMapDrawable)
        }
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null){
            Log.d("AgregarClienteFragment", "La foto fue tomada")

            selectedImagePath = data.data
            val selectedImageBmp= MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedImagePath)
            val bitMapDrawable = BitmapDrawable(selectedImageBmp)
            agregar_foto_cliente.setImageDrawable(bitMapDrawable)
        }
    }

    private fun addClient(nombre: String = "", apellido: String = "", direccion: String = "", telefono: String = "", fotoPerfil: String? =null){
        val userFieldMap = mutableMapOf<String, Any>()
        if (nombre.isNotBlank()) userFieldMap["name"] = nombre
        if (apellido.isNotBlank()) userFieldMap["apellido"] = apellido
        if (direccion.isNotBlank()) userFieldMap["direccion"] = direccion
        if (telefono.isNotBlank()) userFieldMap["telefono"] = telefono
        if (fotoPerfil!= null) userFieldMap["fotoPerfil"] = fotoPerfil
        clientesDBRef.add(userFieldMap)
            .addOnSuccessListener {
                val toast = Toast.makeText(activity, "Cliente agregado", Toast.LENGTH_SHORT)
                toast.show()
                activity!!.supportFragmentManager.popBackStackImmediate()
                Log.d("Agregar", "Se agrego el cliente ")
            }
            .addOnFailureListener {
                val toast = Toast.makeText(activity, "No se pudo agregar al cliente, intenta otra vez", Toast.LENGTH_SHORT)
                toast.show()
            }
    }

    private fun setClientDB(){
        clientesDBRef= store.collection("newclient")
    }

    private fun uploadImageToFirebaseStorage(){
        if (selectedImagePath== null) return
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/fotosPerfil/$filename")

        ref.putFile(selectedImagePath!!)
            .addOnSuccessListener {
                Log.d("Agregar", "Se agrego bien la imagen: ${it.metadata?.path}")
                ref.downloadUrl.addOnSuccessListener {value ->
                    value.toString()
                    Log.d("AgregarFoto", "FileLocation: $value")
                    saveCliente(value.toString())
                }
            }
    }
    private fun saveCliente(imagenPerfil: String) {
        if(editText_nombre== null || editText_apellido == null || editText_telefono == null || editText_direccion == null ){
            val toast = Toast.makeText(activity, "Por favor ingresa los datos", Toast.LENGTH_SHORT)
            toast.show()
        }
        else{
            addClient(editText_nombre.text.toString(),
                editText_apellido.text.toString(), editText_direccion.text.toString(),
                editText_telefono.text.toString(), imagenPerfil)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            AgregarClienteFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}


