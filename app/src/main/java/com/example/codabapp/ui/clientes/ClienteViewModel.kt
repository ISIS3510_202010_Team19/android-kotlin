package com.example.codabapp.ui.clientes

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ClienteViewModel(application: Application): AndroidViewModel(application) {

    //Firebase
    private val store: FirebaseFirestore = FirebaseFirestore.getInstance()

    val allClientes: MutableLiveData<List<Cliente>> = MutableLiveData<List<Cliente>>()

    private var cntClientes = 0

    init {
        store.collection("newclient")
            .addSnapshotListener { value, e ->
                if(e != null)
                {
                    //Error consultando los clientes
                    Log.d("Pablo", "ClienteViewModel - init - ${e.message}")
                }
                var nombre: String
                var apellido: String
                var direccion: String
                var telefono: String
                var fotoPerfil: String

                val clientes: MutableList<Cliente> = mutableListOf()
                for (d in value!!) {
                    nombre = d.data["name"].toString()
                    apellido = d.data["apellido"].toString()
                    direccion = d.data["direccion"].toString()
                    telefono = d.data["telefono"].toString()
                    fotoPerfil = d.data["fotoPerfil"].toString()
                    clientes.add(
                        Cliente(nombre, apellido, direccion, telefono, fotoPerfil)
                    )
                    cntClientes++
                }
                allClientes.value = clientes
            }
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(cliente: Cliente) = viewModelScope.launch(Dispatchers.IO) {
        val data = hashMapOf(
            "apellido" to cliente.apellido,
            "direccion" to cliente.direccion,
            "name" to cliente.nombre,
            "telefono" to cliente.telefono
        )
        store.collection("newclient")
            .add(data)
        cntClientes++
    }

    /**
     * Getting a client based on name and lastname
     */
    fun getClientByName(fullName: String): Cliente? {
        var ans: Cliente? = null
        var name : String
        for(c in allClientes.value!!)
        {
            name = c.nombre + " " + c.apellido
            if(fullName == name)
            {
                ans = c
                break
            }
        }
        return ans
    }

    fun getClientByPosition(pos: Int): Cliente {
        return allClientes.value!![pos]
    }
}