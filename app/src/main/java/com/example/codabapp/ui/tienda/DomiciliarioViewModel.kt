package com.example.codabapp.ui.tienda

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DomiciliarioViewModel(application: Application): AndroidViewModel(application) {

    //Firebase
    private val store: FirebaseFirestore = FirebaseFirestore.getInstance()

    val allDomiciliarios: MutableLiveData<List<Domiciliario>> = MutableLiveData<List<Domiciliario>>()

    private var cntDomiciliarios = 0

    init {
        store.collection("domiciliario")
            .addSnapshotListener { value, e ->
                if(e != null)
                {
                    //Error consultando los clientes
                    Log.d("Mariana", "DomiciliarioViewModel - init - ${e.message}")
                }
                var nombre: String
                var apellido: String
                var telefono: String
                var fotoPerfil: String

                val domiciliarios: MutableList<Domiciliario> = mutableListOf()
                for (d in value!!) {
                    nombre = d.data["name"].toString()
                    apellido = d.data["apellido"].toString()

                    telefono = d.data["telefono"].toString()
                    fotoPerfil = d.data["fotoPerfil"].toString()
                    domiciliarios.add(
                        Domiciliario(nombre, apellido, telefono,fotoPerfil)
                    )
                    cntDomiciliarios++
                }
                allDomiciliarios.value = domiciliarios
            }
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(domiciliario: Domiciliario) = viewModelScope.launch(Dispatchers.IO) {
        val data = hashMapOf(
            "apellido" to domiciliario.apellido,

            "name" to domiciliario.nombre,
            "telefono" to domiciliario.telefono,
            "fotoPerfil" to domiciliario.fotoPerfil
        )
        store.collection("domiciliario")
            .add(data)
        cntDomiciliarios++
    }

    fun getDomiciliarioByPos(pos: Int): Domiciliario{
        return allDomiciliarios.value!![pos]
    }
}