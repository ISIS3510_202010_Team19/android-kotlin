package com.example.codabapp.ui.pedidos

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.codabapp.R
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codabapp.ui.MainEmptyActivity
import com.example.codabapp.ui.clientes.ClienteViewModel
import com.example.codabapp.ui.pedidos.PedidoListAdapter.OnPedidoListener
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_pedidos.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class PedidosFragment : Fragment(), OnPedidoListener {

    //List adapter para el RecyclerView de pedidos
    private lateinit var adapter: PedidoListAdapter

    // Pedido ViewModel
    private lateinit var pedidoViewModel: PedidoViewModel

    //Cliente ViewModel para recuperar las fotos de perfil
    private lateinit var clienteViewModel: ClienteViewModel

    //Auth
    private val mAuth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }

    //AgregarPedido Activity Code
    private val AGREGAR_PEDIDO_CODE = 77

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        //Inicializar los ViewModels
        pedidoViewModel = ViewModelProvider(this).get(PedidoViewModel::class.java)
        clienteViewModel = ViewModelProvider(this).get(ClienteViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //Logout click listener
        btn_logout.setOnClickListener {
            signOut()
        }

        //Agregar pedido
        crear_pedido.setOnClickListener{
            startActivityForResult(Intent(context, AgregarPedido::class.java), AGREGAR_PEDIDO_CODE)
        }

        //RecyclerView
        val recyclerView = recyclerview_pedidos
        adapter = PedidoListAdapter(this.context!!, this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this.context!!)

        pedidoViewModel.allPedidos.observe(viewLifecycleOwner, Observer { pedidos ->
            pedidos?.let{adapter.setPedidos(it)}
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pedidos, container, false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == AGREGAR_PEDIDO_CODE)
        {
            Log.d("Pablo", "Volvió de agregar pedido - code: $requestCode")
            adapter.notifyDataSetChanged()
        }
    }

    private fun signOut(){
        mAuth.signOut()

        val intent = Intent(context, MainEmptyActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    override fun onPedidoClick(pos: Int) {
        val sel = pedidoViewModel.getPedidoByPosition(pos)
        val cliente = clienteViewModel.getClientByName(sel.cliente)

        val intent = Intent(context, DetallePedido::class.java)
        intent.putExtra("id", sel.id)
        intent.putExtra("name", sel.cliente)
        intent.putExtra("direccion", cliente?.direccion)
        intent.putExtra("telefono", cliente?.telefono)
        intent.putExtra("foto", sel.fotoPerfil)
        intent.putExtra("pedido", sel.texto)
        intent.putExtra("estado", sel.estado)
        intent.putExtra("productos", sel.productos)
        startActivity(intent)

        adapter.notifyItemChanged(pos)
    }
}
