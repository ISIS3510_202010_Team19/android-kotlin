package com.example.codabapp.ui.pedidos

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PedidoViewModel(application: Application) : AndroidViewModel(application) {

    //Firebase
    private val store: FirebaseFirestore = FirebaseFirestore.getInstance()
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allPedidos: MutableLiveData<List<Pedido>> = MutableLiveData<List<Pedido>>()

    private var cntPedidos = 0

    init {
        store.collection("pedidos")
            .addSnapshotListener{ value, e ->
                if(e != null)
                {
                    //Error en la lectura
                    Log.d("Pablo", "PedidoViewModel - init - ${e.message}")
                    return@addSnapshotListener
                }
                var id: String
                var clienteActual: String
                var textoActual: String
                var fotoCliente: String?
                var estado: String
                var productos: String
                val pedidos: MutableList<Pedido> = mutableListOf()
                for (d in value!!) {
                    id = d.id
                    clienteActual = d.data["cliente"].toString()
                    textoActual = d.data["texto"].toString()
                    fotoCliente = d.data["fotoCliente"].toString()
                    estado = d.data["estado"].toString()
                    productos = d.data["productos"].toString()
                    pedidos.add(
                        Pedido(
                            id,
                            clienteActual,
                            textoActual,
                            fotoCliente,
                            estado,
                            productos
                        )
                    )
                    cntPedidos++
                }
                allPedidos.value = pedidos
            }
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(pedido: Pedido) = viewModelScope.launch(Dispatchers.IO) {
        val data = hashMapOf(
            "cliente" to pedido.cliente,
            "texto" to pedido.texto,
            "fotoCliente" to pedido.fotoPerfil,
            "estado" to pedido.estado,
            "productos" to pedido.productos
        )
        store.collection("pedidos")
            .add(data)
        cntPedidos++
    }

    fun getPedidoByPosition(pos: Int): Pedido {
        return allPedidos.value!![pos]
    }

    fun actualizarEstadoPedido(pId: String, pEstado: String) {
        Log.d("Pablo", "actualizarEstadoPedido id->$pId pEstado->$pEstado")
        store.collection("pedidos").document(pId).update("estado", pEstado)
    }

}