package com.example.codabapp.ui.pedidos

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codabapp.R
import com.example.codabapp.ui.utils.CircleTransform
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detalle_pedido.*

class DetallePedido : AppCompatActivity() {

    //Atributos del pedido
    private lateinit var id: String
    private lateinit var nombre: String
    private lateinit var direccion: String
    private lateinit var telefono: String
    private lateinit var foto: String
    private lateinit var pedido: String
    private lateinit var estado: String
    private lateinit var productos: String

    // Pedido ViewModel
    private lateinit var pedidoViewModel: PedidoViewModel

    //List adapter para el RecyclerView de productos
    private lateinit var adapter: ProductosListAdapter

    //Lista de producto con número concatenados para el RecyclerView
    private var listProductos: MutableLiveData<List<String>> = MutableLiveData<List<String>>()

    private fun cambiarEstado(pEstado: String){
        estado = pEstado

        pedidoViewModel.actualizarEstadoPedido(id, pEstado)

        when (estado) {
            "RECIBIDO" -> {
                estado_recibidoaregistrado.setBackgroundColor(Color.parseColor("#8C8C8C"))
                btn_registrado.setImageResource(R.drawable.ic_estado_noregistrado)
                estado_registradoacompletado.setBackgroundColor(Color.parseColor("#8C8C8C"))
                btn_completado.setImageResource(R.drawable.ic_estado_nocompletado)
            }
            "REGISTRADO" -> {
                estado_recibidoaregistrado.setBackgroundColor(Color.parseColor("#00FF63"))
                btn_registrado.setImageResource(R.drawable.ic_estado_registrado)
                estado_registradoacompletado.setBackgroundColor(Color.parseColor("#8C8C8C"))
                btn_completado.setImageResource(R.drawable.ic_estado_nocompletado)
            }
            "COMPLETADO" -> {
                estado_recibidoaregistrado.setBackgroundColor(Color.parseColor("#00FF63"))
                btn_registrado.setImageResource(R.drawable.ic_estado_registrado)
                estado_registradoacompletado.setBackgroundColor(Color.parseColor("#00FF63"))
                btn_completado.setImageResource(R.drawable.ic_estado_completado)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_pedido)

        // Recuperar instancia del viewModel para actualizar el estado del pedido
        pedidoViewModel = ViewModelProvider(this).get(PedidoViewModel::class.java)

        id = intent.extras!!.get("id") as String
        nombre = intent.extras!!.get("name") as String
        direccion = intent.extras!!.get("direccion") as String
        telefono = intent.extras!!.get("telefono") as String
        foto = intent.extras!!.get("foto") as String
        pedido = intent.extras!!.get("pedido") as String
        estado = intent.extras!!.get("estado") as String
        productos = intent.extras!!.get("productos") as String

        nombre_detallePedido.text = nombre
        direccion_detallePedido.text = direccion
        telefono_detallePedido.text = telefono
        pedido_detalle.text = pedido
        Picasso.get().load(foto).resize(120,120)
            .centerCrop().transform(CircleTransform()).into(iconDetallePedido)

        // Pintar el detalle
        if(estado == "REGISTRADO")
        {
            estado_recibidoaregistrado.setBackgroundColor(Color.parseColor("#00FF63"))
            btn_registrado.setImageResource(R.drawable.ic_estado_registrado)
        }
        else if(estado == "COMPLETADO")
        {
            estado_recibidoaregistrado.setBackgroundColor(Color.parseColor("#00FF63"))
            btn_registrado.setImageResource(R.drawable.ic_estado_registrado)
            estado_registradoacompletado.setBackgroundColor(Color.parseColor("#00FF63"))
            btn_completado.setImageResource(R.drawable.ic_estado_completado)
        }

        btn_recibido.setOnClickListener {
            cambiarEstado("RECIBIDO")
        }

        btn_registrado.setOnClickListener {
            cambiarEstado("REGISTRADO")
        }

        btn_completado.setOnClickListener {
            cambiarEstado("COMPLETADO")
        }

        agregarProductosALista()

        //RecyclerView de los productos a ordenar
        val recyclerView = recyclerview_productos_detallePedido
        adapter = ProductosListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        listProductos.observe(this, Observer{productos ->
            productos?.let{adapter.setProductos(it)}
        })
    }

    //Toma el texto interpretado del pedido y arma una lista para armar el contenido del RecyclerView
    private fun agregarProductosALista() {
        val textSplit = productos.split(" - ") //Cada producto está separado por este patrón
        val auxList: MutableList<String> = mutableListOf()
        for(t in textSplit)
            auxList.add(t)
        listProductos.value = auxList
    }
}
