package com.example.codabapp.ui.pedidos

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codabapp.R
import com.example.codabapp.ui.utils.CircleTransform
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recyclerview_item_pedido.view.*

class PedidoListAdapter internal constructor (context: Context, pPedidoListener: OnPedidoListener):
                        RecyclerView.Adapter<PedidoListAdapter.PedidoViewHolder>(){

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var pedidos = emptyList<Pedido>()     //Cached copy of pedidos

    private val onPedidoListener: OnPedidoListener = pPedidoListener

    inner class PedidoViewHolder(itemView: View, pPedidoListener: OnPedidoListener):
        RecyclerView.ViewHolder(itemView), OnClickListener {

        val pedidoClienteNombre: TextView = itemView.pedido_nombre_cliente
        val pedidoId: TextView = itemView.pedido_id
        val iconPedidoCliente: ImageView = itemView.iconPedidoCliente

        // Manejo completo del estado
        val rv_barra1: View = itemView.rv_estado_recibidoaregistrado
        val rv_registrado: ImageView = itemView.rv_estado_registrado
        val rv_barra2: View = itemView.rv_estado_registradoacompletado
        val rv_completado: ImageView = itemView.rv_estado_completado

        private val onPedidoListener: OnPedidoListener = pPedidoListener

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onPedidoListener.onPedidoClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PedidoViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item_pedido, parent, false)
        return PedidoViewHolder(itemView, onPedidoListener)
    }

    override fun onBindViewHolder(holder: PedidoViewHolder, position: Int){
        val current = pedidos[position]
        holder.pedidoClienteNombre.text = current.cliente
        holder.pedidoId.text = current.texto
        Picasso.get().load(current.fotoPerfil).resize(100,100)
            .centerCrop().transform(CircleTransform()).into(holder.iconPedidoCliente)

        // Pintar el estado
        when (current.estado) {
            "RECIBIDO" -> {
                holder.rv_barra1.setBackgroundColor(Color.parseColor("#8C8C8C"))
                holder.rv_registrado.setImageResource(R.drawable.ic_estado_noregistrado)
                holder.rv_barra2.setBackgroundColor(Color.parseColor("#8C8C8C"))
                holder.rv_completado.setImageResource(R.drawable.ic_estado_nocompletado)
            }
            "REGISTRADO" -> {
                holder.rv_barra1.setBackgroundColor(Color.parseColor("#00FF63"))
                holder.rv_registrado.setImageResource(R.drawable.ic_estado_registrado)
                holder.rv_barra2.setBackgroundColor(Color.parseColor("#8C8C8C"))
                holder.rv_completado.setImageResource(R.drawable.ic_estado_nocompletado)
            }
            "COMPLETADO" -> {
                holder.rv_barra1.setBackgroundColor(Color.parseColor("#00FF63"))
                holder.rv_registrado.setImageResource(R.drawable.ic_estado_registrado)
                holder.rv_barra2.setBackgroundColor(Color.parseColor("#00FF63"))
                holder.rv_completado.setImageResource(R.drawable.ic_estado_completado)
            }
        }
    }

    internal fun setPedidos(pedidos:List<Pedido>){
        this.pedidos = pedidos
        notifyDataSetChanged()
    }

    override fun getItemCount() = pedidos.size

    //Fuente https://youtu.be/69C1ljfDvl0
    interface OnPedidoListener{
        fun onPedidoClick(pos: Int)
    }

}