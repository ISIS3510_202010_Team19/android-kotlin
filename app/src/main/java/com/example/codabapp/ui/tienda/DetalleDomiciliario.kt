package com.example.codabapp.ui.tienda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.codabapp.R
import com.example.codabapp.ui.utils.CircleTransform
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detalle_domiciliario.*

class DetalleDomiciliario : AppCompatActivity() {

    private lateinit var nombre: String
    private lateinit var telefono: String
    private lateinit var foto: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_domiciliario)

        nombre = intent.extras!!.get("name") as String
        telefono = intent.extras!!.get("telefono") as String
        foto = intent.extras!!.get("foto") as String

        nombre_detalleDomiciliario.text = nombre
        telefono_detalleDomiciliario.text = telefono
        Picasso.get().load(foto).resize(120,120)
            .centerCrop().transform(CircleTransform()).into(iconDetalleDomiciliario)
    }
}