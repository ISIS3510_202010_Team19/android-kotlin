package com.example.codabapp.ui.pedidos

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codabapp.R
import kotlinx.android.synthetic.main.activity_agregar_pedido.*
import kotlinx.android.synthetic.main.recyclerview_item_productos.view.*

class ProductosListAdapter internal constructor (context: Context):
    RecyclerView.Adapter<ProductosListAdapter.ProductoViewHolder>(){

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var productos = emptyList<String>()     //Cached copy of productos

    inner class ProductoViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val productoView: TextView = itemView.producto_view
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductoViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item_productos, parent, false)
        return ProductoViewHolder(itemView)
    }

    internal fun setProductos(pProductos: List<String>){
        productos = pProductos
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return productos.size
    }

    override fun onBindViewHolder(holder: ProductoViewHolder, position: Int) {
        val current = productos[position]
        holder.productoView.text = current
    }

}