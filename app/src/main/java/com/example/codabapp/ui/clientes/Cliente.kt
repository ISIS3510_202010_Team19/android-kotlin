package com.example.codabapp.ui.clientes

data class Cliente(
    var nombre: String,
    var apellido: String,
    var direccion: String,
    var telefono: String,
    var fotoPerfil: String?
) {
    constructor(): this("","","","", null)
}