package com.example.codabapp.ui.utils


import android.app.Activity
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import java.util.regex.Pattern


fun ViewGroup.inflate(layoutId: Int)= LayoutInflater.from(context).inflate(layoutId, this, false )!!
fun Activity.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) = Toast.makeText(this,message,duration)

//fun ImageView.loadByResource(resource: Int) = Picasso.with(context).load(resource).fit().into(this)

inline fun <reified T:Activity> Activity.goToActivity(noinline init:Intent.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    intent.init()
    startActivity(intent)
}


fun EditText.validate(validation: (String) -> Unit){
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            validation(editable.toString())
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }


    })
}

fun Activity.isValidateEmail(email: String): Boolean{
    val pattern = Patterns.EMAIL_ADDRESS
    return pattern.matcher(email).matches()
}
fun Activity.isValidatePassword(password: String): Boolean{

    val passwordPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"
    val pattern = Pattern.compile(passwordPattern)
    return pattern.matcher(password).matches()
}
fun Activity.isValidateConfirmPassword(password: String, confirmPassword: String): Boolean{
    return password== confirmPassword
}


