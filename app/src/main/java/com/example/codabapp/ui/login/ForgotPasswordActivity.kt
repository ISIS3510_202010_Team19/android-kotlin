package com.example.codabapp.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.codabapp.R
import com.example.codabapp.ui.utils.goToActivity
import com.example.codabapp.ui.utils.isValidateEmail
import com.example.codabapp.ui.utils.validate
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_sing_up.button_backLoggin

class ForgotPasswordActivity : AppCompatActivity() {

    private val mAuth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        editText_emailforgot.validate {
            editText_emailforgot.error= if (isValidateEmail(it)) null else "El email no es válido"
        }


        button_backLoggin.setOnClickListener {
            goToActivity<LoginActivity>{
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }

        button_forgot.setOnClickListener {
            val email = editText_emailforgot.text.toString()
            if(isValidateEmail(email)){
                mAuth.sendPasswordResetEmail(email).addOnCompleteListener(this){
                    Toast.makeText(this, "El email ha sido enviado para reestablecer la contraseña", Toast.LENGTH_LONG).show()
                    goToActivity<LoginActivity>{
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    }
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }

            }else{
                Toast.makeText(this, "Por favor verifica que el correo sea correcto", Toast.LENGTH_LONG).show()
            }
        }
    }
}

