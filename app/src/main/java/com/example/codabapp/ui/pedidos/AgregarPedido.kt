package com.example.codabapp.ui.pedidos

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codabapp.R
import com.example.codabapp.ui.clientes.ClienteViewModel
import com.example.codabapp.ui.utils.CircleTransform
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_agregar_pedido.*
import java.lang.Exception
import java.util.ArrayList

class AgregarPedido : AppCompatActivity() {

    //Nombres de clientes para el spinner
    private var clientes: MutableList<String> = mutableListOf("Escoge un cliente")

    //Firebase
    private val store: FirebaseFirestore = FirebaseFirestore.getInstance()

    //List adapter para el RecyclerView de productos
    private lateinit var adapter: ProductosListAdapter

    //Pedido ViewModel
    private lateinit var pedidoViewModel: PedidoViewModel

    //Cliente ViewModel
    private lateinit var clienteViewModel: ClienteViewModel

    //Código para el reconocimiento de voz
    private val requestCodeSpeechInput = 100

    //Lista de productos que se quieren pedir y lista de cuántos de cada uno se quieren
    //Ej: productos(0)="papa" y cntProductos(0)=5 indica que el pedido incluye 5 unidades de papa
    private var productos: MutableList<String> = mutableListOf()
    private var cntProductos: MutableList<String> = mutableListOf()

    //Lista de producto con número concatenados para el RecyclerView
    private var listProductos: MutableLiveData<List<String>> = MutableLiveData<List<String>>()

    //Lista que determina los productos que pueden ser agregados a la lista de productos
    private val posiblesProductos: List<String> = listOf("papa", "manzana", "kiwi", "aguacate", "calabacín",
                                                        "papaya", "huevo", "coco", "zanahoria", "pepino",
                                                        "pera", "mango", "lechuga", "tomate", "cebolla",
                                                        "durazno", "plátano", "piña", "uva", "calabaza",
                                                        "banana", "banano", "lulo", "espinaca", "fresa",
                                                        "naranja", "mandarina", "toronja", "yuca")

    //Esta lista es necesaria porque a veces se toman los número como letras y no en dígitos
    //La lista llega hasta diez porque, según lo que vimos en pruebas, solo tiende a poner en letras
    //a los números menores a diez
    private val posiblesNumeros: List<String> = listOf("Un", "un", "Una", "una", "Dos", "dos", "Tres", "tres",
                                                        "Cuatro", "cuatro", "Cinco", "cinco", "Seis", "seis",
                                                        "Siete", "siete", "Ocho", "ocho", "Nueve", "nueve",
                                                        "Diez", "diez")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_pedido)

        pedidoViewModel = ViewModelProvider(this).get(PedidoViewModel::class.java)
        clienteViewModel = ViewModelProvider(this).get(ClienteViewModel::class.java)

        btn_pedido.setOnClickListener{
            speak()
        }

        btn_agregar_pedido.setOnClickListener{
            agregarPedido()
        }

        //Agregar clientes al spinner
        store.collection("newclient")
            .addSnapshotListener { value, e ->
                if(e != null)
                {
                    //Error consultando los clientes
                    Log.d("Pablo", "ClienteViewModel - init - ${e.message}")
                }
                clientes = mutableListOf("Escoge un cliente")
                for (d in value!!) {
                    clientes.add("${d.data["name"].toString()} ${d.data["apellido"].toString()}")
                }

                val spinner = spinner_clientes

                val obj = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
                    {
                        lbl_cliente_escogido.text = (parent!!.getItemAtPosition(position).toString())

                        if(lbl_cliente_escogido.text.toString() != "Escoge un cliente") {
                            val foto = clienteViewModel.getClientByName(lbl_cliente_escogido.text.toString())?.fotoPerfil
                            Picasso.get().load(foto).resize(100, 100)
                                .centerCrop().transform(CircleTransform())
                                .into(icon_cliente_escogido)
                        }
                        else {
                            icon_cliente_escogido.setImageResource(R.drawable.ic_account)
                        }
                    }
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        //Do nothing(?)
                    }
                }

                spinner.onItemSelectedListener = obj
                spinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, clientes)
            }

        //RecyclerView de los productos a ordenar
        val recyclerView = recyclerview_productos
        adapter = ProductosListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        listProductos.observe(this, Observer{productos ->
            productos?.let{adapter.setProductos(it)}
        })
    }

    //Se llama al presionar el micrófono
    private fun speak() {
        val mIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        mIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        mIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "es-MX")      //Idioma de speech to text
        try {
            startActivityForResult(mIntent, requestCodeSpeechInput)
        } catch (e: Exception) {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
    }

    //Lo que ocurre luego de dictar el pedido
    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            requestCodeSpeechInput -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val result: ArrayList<String>? =
                        data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)

                    //Texto del pedido
                    text_pedido.text = "\"${result!![0]}\""

                    //Reiniciar el pedido
                    productos = mutableListOf()
                    cntProductos = mutableListOf()
                    listProductos.value = mutableListOf()

                    //Agregar los productos y las cantidades a las listas correspondientes
                    val palabras = text_pedido.text.toString().replace("\"", "")
                    procesarPalabras(palabras.split(" "))
                }
            }
        }
    }

    private fun procesarPalabras(palabras: List<String>){
        var actual: String
        var siguiente: String
        for(i in 0 until palabras.size-1) {

            actual = palabras[i]
            siguiente = palabras[i+1]

            if(actual.isDigitsOnly() || posiblesNumeros.contains(actual)) {//Es un número válido
                if(esProductoValido(siguiente)) { //Es un producto válido
                    productos.add(siguiente)
                    cntProductos.add(actual)
                }

            }
        }
        val listaAux: MutableList<String> = mutableListOf()
        for(i in 0 until productos.size){
            if(cntProductos[i].isDigitsOnly())
                listaAux.add("${cntProductos[i]} ${productos[i]}")
            else{
                listaAux.add("${deLetrasANumeros(cntProductos[i])} ${productos[i]}")
            }
        }
        listProductos.value = listaAux
        adapter.notifyDataSetChanged()
    }

    //Entra un numero escrito en letras y sale el numero en sí
    //A veces el speech-to-text interpreta los números menores a diez en texto (e.g. "ocho" en vez de "8")
    private fun deLetrasANumeros(param: String): String{
        var ans = ""

        when(param) {
            "Una", "una", "Un", "un" -> ans = "1"
            "Dos", "dos" -> ans = "2"
            "Tres", "tres" -> ans = "3"
            "Cuatro", "cuatro" -> ans = "4"
            "Cinco", "cinco" -> ans = "5"
            "Seis", "seis" -> ans = "6"
            "Siete", "siete" -> ans = "7"
            "Ocho", "ocho" -> ans = "8"
            "Nueve", "nueve" -> ans = "9"
            "Diez", "diez" -> ans = "10"
        }

        return ans
    }

    //Indica si el parámetro es un producto válido
    private fun esProductoValido(param: String): Boolean {
        var ans = false

        for(p in posiblesProductos){
            if(param.startsWith(p)) //Se usa startsWith para tener en cuenta plurales (e.g. "manzanas")
                ans = true
        }

        return ans
    }

    //Se intenta agregar el pedido
    private fun agregarPedido(){
        val pedido = text_pedido.text.toString()
        val nombre_cliente = lbl_cliente_escogido.text.toString()
        val foto = clienteViewModel.getClientByName(nombre_cliente)?.fotoPerfil

        //Se verifica que le pedido sea válido
        if (nombre_cliente == "Escoge un cliente") {
            Toast.makeText(
                this,
                "¡Selecciona un cliente de la lista antes de dictar el pedido!",
                Toast.LENGTH_LONG
            ).show()
        }
        else if(pedido.isEmpty() || pedido.isBlank() || productos.isEmpty()) {
            Toast.makeText(
                this,
                "¡Asegúrate de haber dictado un pedido válido!",
                Toast.LENGTH_LONG
            ).show()
        }
        else {
            //Si es válido, se agrega
            var pedido_aux = ""
            val productosDictados = listProductos.value!!
            for(i in 0 until productosDictados.size-1)
                pedido_aux += "${productosDictados[i]} - "
            pedido_aux += productosDictados[productosDictados.size-1]

            val newPedido = Pedido(null, nombre_cliente, pedido, foto, "RECIBIDO", pedido_aux)
            pedidoViewModel.insert(newPedido)
            Toast.makeText(
                this,
                "Se agregó un nuevo pedido para $nombre_cliente",
                Toast.LENGTH_LONG
            ).show()
            //Termina la actividad
            finish()
        }
    }
}