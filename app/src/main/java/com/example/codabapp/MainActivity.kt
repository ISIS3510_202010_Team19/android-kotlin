package com.example.codabapp

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
// import androidx.navigation.ui.AppBarConfiguration

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.codabapp.ui.clientes.ClientesFragment
import com.example.codabapp.ui.pedidos.PedidosFragment
import com.example.codabapp.ui.tienda.DomiciliarioFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    //Navigation------------------------------------------------------------------------------------

    private var fragment1: Fragment = PedidosFragment()
    private var fragment2: Fragment = DomiciliarioFragment()
    private var fragment3: Fragment = ClientesFragment()
    private var fm: FragmentManager = supportFragmentManager
    private var active: Fragment = fragment1

    private val mOnNavigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener
    = BottomNavigationView.OnNavigationItemSelectedListener {

        var ans = false

        when (it.itemId) {
            R.id.navigation_pedidos -> {
                fm.beginTransaction().hide(active).show(fragment1).commit()
                active = fragment1
                ans = true
            }
            R.id.navigation_tienda -> {
                fm.beginTransaction().hide(active).show(fragment2).commit()
                active = fragment2
                ans = true
            }
            R.id.navigation_clientes -> {
                fm.beginTransaction().hide(active).show(fragment3).commit()
                active = fragment3
                ans = true
            }
        }

        return@OnNavigationItemSelectedListener ans
    }

    //----------------------------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Navigation
        //Fuente https://medium.com/@oluwabukunmi.aluko/bottom-navigation-view-with-fragments-a074bfd08711
        nav_view.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        fm.beginTransaction().add(R.id.nav_host_fragment, fragment3, "3").hide(fragment3).commit()
        fm.beginTransaction().add(R.id.nav_host_fragment, fragment2, "2").hide(fragment2).commit()
        fm.beginTransaction().add(R.id.nav_host_fragment, fragment1, "1").hide(fragment1).commit()
        fm.beginTransaction().show(active).commit()
    }

}


